## Instructions

Please fork this repository, check it out, commit your changes to it, and send us the URL.

 * `composer install`
 * `php -S localhost:8000 -t public`
 * Open this in your browser: `http://localhost:8000`
 * Keep following these instructions.

## Requirements
* PHP 7+
* Composer

