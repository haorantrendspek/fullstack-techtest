<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

	private function assets()
	{
		return [
			1 => [
				'id' => 1,
				'name' => 'Waverley Reservoir',
				'country' => 'Australia',
				'type' => 'reservoir',
				'latitude' => -33.89480,  
				'longitude' => 151.25699,
				'date_created' => 1536139914,
			],
			2 => [
				'id' => 2,
				'name' => 'Canal Demo',
				'country' => 'Australia',
				'latitude' => -33.84538,  
				'longitude' => 150.87295,
				'date_created' => 1534224273,
				'type' => 'canal',
			],
			3 => [
				'id' => 3,
				'name' => 'Freestanding Reservoir',
				'country' => null,
				'type' => 'reservoir',
				'latitude' => -33.73739,  
				'longitude' => 150.78922,
				'date_created' => 1537488000,
			],
			4 => [
				'id' => 4,
				'name' => 'Lake Charles',
				'country' => 'United States',
				'type' => 'reservoir',
				'latitude' => 30.11308,
				'longitude' => -93.28801,
				'date_created' => 1567992565,
			],
			5 => [
				'id' => 5,
				'name' => 'EX Substation',
				'country' => null,
				'type' => 'substation',
				'latitude' => -33.73739,  
				'longitude' => 150.78922,
				'date_created' => 1530488000,
			],
			6 => [
				'id' => 6,
				'name' => 'EX Plant',
				'country' => 'South Africa',
				'type' => 'reservoir',
				'latitude' => "-25.83836",
				'longitude' => 29.98707,
				'date_created' => 1567092565,
			],
		];
	}


    public function showAssets()
    {
    	return response()->json($this->assets());
    }

    public function showAsset($id)
    {
    	$assets = $this->assets();

    	return response()->json($assets[$id]);
    }

    public function home()
    {
    	return view('home');
    }
}
