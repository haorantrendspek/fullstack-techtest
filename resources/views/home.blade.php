<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trendspek Technical Test</title>
  </head>
  <body>

	<div class="container">
	    <h1>Hello, Trendspek!</h1>


	    <h3>Introduction</h3>
	    <p>
	    	This application contains a very basic API. (Look at <code>app/Http/Controllers/Controller.php</code> for some terrible hardcoding).
	    	It also contains some extremely basic Javascript (view source and see below).
		</p><p>
	    	Your mission is to convert this into something that works. Bonus points for:
	    	<ul>
	    		<li>elegant design (front-end and backend)</li>
	    		<li>making this more beautiful (for the record, Trendspek orange is <span style="color: #f26721">#f26721</span> and Trendspek grey is <span style="color: #2e303a">#2e303a</span>)</li>
	    		<li>code quality</li><li>Opinionated coding.</li>
	    		<li>defensive coding (show me what happens if the API fails to connect!)</li>
	    		<li>handling the API correctly</li>
	    		<li>writing some tests (unit, browser)</li>
	    	</ul>
	    	You may change the backend (e.g. Controller.php) or the front-end (this file, and others). You may create any files as necessary. You may delete any files or code as necessary. You may use a JS frameworks you want. <b>Do <em>not</em> use a jQuery plugin like Datatables, because that would make things boring.</b>
	    </p>
	    <p>Please document, and describe how to install and run your code, as necessary.</p>

	    <h3>Instructions: Part 1</h3>
	    <p>
	    	Please render this in a proper table. If you want to be fancy, you can be fancy.
	    </p>

	    <h3>Instructions: Part 2</h3>
	    <p>
	    	<ul>
	    		<li>Add a header row.</li>
	    		<li>Click once on a header-row to sort by that header</li>
	    		<li>Click again to sort in the reverse order</li>
	    	</ul>
	    </p>

	    <h3>Instructions: Part 3</h3>
	    <p>Add filtering. Create some way to show only certain rows on certain conditions, e.g. filtering by name, or asset type. You may do this in front-end
	    	or back-end.
	    </p>

	    <hr>

	    <div class="table-container">
	    	<table>
	    		<tr>
	    			<td>
	    				<pre id="raw-data"></pre>
	    			</td>
	    		</tr>
	    	</table>
	    </div>


	</div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>

    	$.get('/assets').then(function (d) {
    		$('#raw-data').html(
    			JSON.stringify(d)
    			);
    	});
    </script>
  </body>
</html>